/*
 * Copyright 2016-2017 adesso AG
 * Copyright 2011-2016 VX4.NET
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */
package net.vx4.app.mobileIDSE.hal.usb;

import java.util.*;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.util.Log;
import com.android.internal.util.Predicate;

/**
 * @author Christian Kahlo, ck@vx4.de
 *
 */
public final class UsbDeviceMgr {

	private static final String ACTION_USB_PERMISSION = UsbDeviceMgr.class.getName() + ".USB_PERMISSION";

    private final List<UsbDevice> accessibleDevices = new ArrayList<>();
    private final Map<UsbDevice, UsbBasicDevice> openDevices = new HashMap<>();

	private final Context mContext;
    private final UsbManager mUsbManager;

    /**
     * permission broadcast receiver used to receive permission dialog result
     */
    private final BroadcastReceiver mUsbDeviceAccessReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (ACTION_USB_PERMISSION.equals(intent.getAction())) {
                synchronized (this) {
                    context.unregisterReceiver(mUsbDeviceAccessReceiver);
                    final boolean permission = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false);
                    final UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    Log.d("USBDEVICEACCESS", "permission "  + permission + " received for device " + device);

                    if (permission && device != null) {
                        addDevice(device);
                    }
                }
            }
        }
    };

    /**
     * if device is detached remove it from accessible devices and opened / used devices
     */
    private final BroadcastReceiver mUsbDeviceDetachedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            synchronized (this) {
                final UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    Log.d("USBDEVICEDETACH", ""  + intent.getAction()  + " " + device.getDeviceName());
                    removeDevice(device);
                }
            }
        }
    };

    /**
     * if a new device is attached register and request permissions
     *
     * // add attach / on-request permission filter?
     */
    private final BroadcastReceiver mUsbDeviceAttachedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            synchronized (this) {
                final UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if(device != null) {
                    Log.d("USBDEVICEATTACH", ""  + intent.getAction()  + " " + device.getDeviceName());
                    // check default filter / listener
                    requestPermission();
                }
            }
        }
    };

    // default filters?
    private UsbDeviceMgr(final Context context) {
        this.mContext = context;
        this.mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

        this.mContext.registerReceiver(this.mUsbDeviceDetachedReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));
        this.mContext.registerReceiver(this.mUsbDeviceAttachedReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));

        // TODO: check, request permission for already connected devices
        requestPermission();
    }

    public static UsbDeviceMgr get(final Context context) {
	    return new UsbDeviceMgr(context);
    }

	public void close() {
        try { this.mContext.unregisterReceiver(mUsbDeviceAccessReceiver); } catch(Exception e) {}
		try { this.mContext.unregisterReceiver(mUsbDeviceDetachedReceiver); } catch(Exception e) {}
		try { this.mContext.unregisterReceiver(mUsbDeviceAttachedReceiver); } catch(Exception e) {}

		for(final UsbBasicDevice bd : this.openDevices.values()) {
            try { bd.close(); } catch(Exception e) {}
        }
	}

    private void requestPermission(final UsbDevice device) {
        this.mContext.registerReceiver(this.mUsbDeviceAccessReceiver, new IntentFilter(ACTION_USB_PERMISSION));
        this.mUsbManager.requestPermission(device,
                PendingIntent.getBroadcast(this.mContext, 0, new Intent(ACTION_USB_PERMISSION), 0));
    }

    private void requestPermission(final Predicate<UsbDevice>... filters) {
		for (final UsbDevice device : this.mUsbManager.getDeviceList().values()) {
			if (!this.accessibleDevices.contains(device)) {
			    if(filters == null || filters.length == 0) {
			        requestPermission(device);
                } else {
			        for(final Predicate<UsbDevice> filter : filters) {
			            if(filter.apply(device)) {
                            requestPermission(device);
                        }
                    }
                }
			}
		}
	}

	// add listener for new filtered device successfully added?
	private void addDevice(final UsbDevice device) {
		if (!this.accessibleDevices.contains(device)) {
            synchronized (this.accessibleDevices) {
                this.accessibleDevices.add(device);
                this.accessibleDevices.notifyAll();
            }
        }
	}
	
	private void removeDevice(final UsbDevice device) {
		if (this.accessibleDevices.remove(device)) {
            final UsbBasicDevice usedDevice = this.openDevices.remove(device);
            if(usedDevice != null) {
                usedDevice.close();
            }
		}
	}
	
	public List<UsbDevice> getAccessibleDevices(final boolean force, final Predicate<UsbDevice>... filters) {
	    if(force && this.accessibleDevices.isEmpty() && !this.mUsbManager.getDeviceList().isEmpty()) {
            requestPermission(filters);
            synchronized (this.accessibleDevices) {
                try { this.accessibleDevices.wait(); } catch (InterruptedException e) { }
            }
        }

        return this.accessibleDevices;
	}

	public UsbBasicDevice open(final UsbDevice device, final UsbInterface usbIf) {
	    final UsbBasicDevice bd = new UsbBasicDevice(this.mUsbManager, device, usbIf);
        if(bd.open()) {
            this.openDevices.put(device, bd);
            return bd;
        } else {
            return null;
        }
    }
}